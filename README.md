# aalborg_kv21

Analyse af resultaterne fra KV21 i Aalborg.

Data fra KMD, processeret af Ole Rosenørn de Lasson, Anna Dorthe Berthelsen og Pierre Vogler-Finck.

Figurer kan genereres ved at køre *analyse.py* python filen.

Notater: 

- det nemmeste er at bruge *devcontainer* i VSCode med det sætup der ligger ind i Git repositoriet, og så køre kommandoen:
    > make run

- hvis download af kort skulle give problemer, så er der en backup af de nødvendige filer i *geodata_backup* folderen.
