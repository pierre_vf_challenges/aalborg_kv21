import json
import os

import pandas as pd

TMP_DATA_FOLDER = ".data"
JSON_DATA_FILE = "data/kv21-aalborg.json"

VOTE_DATA_PER_AREA = TMP_DATA_FOLDER + "/votes_by_area.csv"
VOTE_DATA_PER_DISTRICT = TMP_DATA_FOLDER + "/votes_by_district.csv"

try:
    os.mkdir(TMP_DATA_FOLDER)
except:
    pass


def load_data_from_json() -> tuple[pd.DataFrame, pd.DataFrame]:
    """
    Loads the voting results from the JSON file

    :return: dataframe with vote counts per party, dataframe with vote counts differences
    """
    with open(JSON_DATA_FILE, "r") as f:
        d = json.load(f)

    to_postcode = pd.read_csv("data/postcode_per_voting_station.csv", sep=";")

    areas = []
    parties = []
    for a in d["Areas"]:
        areas.append(a["Name"])
        d_i = a["Parties"]
        for d in d_i:
            d["area"] = a["Name"]
        parties.append(d_i)

    df = pd.DataFrame(data=dict(area=areas, party=parties))
    df_exp = pd.DataFrame(df["party"].explode().to_list())
    df_exp.drop(columns=["Candidates", "CandidatesPage"], inplace=True)

    augment_df = lambda x: x.merge(
        to_postcode, how="outer", left_on="area", right_on="voting_station"
    )
    df_votes = augment_df(
        group_votes_by(
            df_exp[["Letter", "VoteCount", "area"]].rename(
                columns={"Letter": "party", "VoteCount": "votes"}
            ),
            "area",
        )
    )
    df_change = augment_df(
        group_votes_by(
            df_exp[["Letter", "VoteChange", "area"]].rename(
                columns={"Letter": "party", "VoteChange": "votes"}
            ),
            "area",
        )
    )
    return df_votes, df_change


def group_votes_by(df: pd.DataFrame, x: str) -> pd.DataFrame:
    df_out = df.groupby(x).sum().rename(columns={"votes": "total_votes"})
    for p in df["party"].unique():
        df_i = df[df["party"] == p].groupby(x).sum().rename(columns={"votes": p})
        df_out = df_out.merge(df_i[[p]], left_index=True, right_index=True)
    return df_out
