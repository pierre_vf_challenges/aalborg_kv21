import pandas as pd
import os
import requests
import json
import plotly.express as px
import plotly.graph_objects as go
import geopandas as gpd

# --------------------------------------------------------------------------------------------------------------------
from data_loading import TMP_DATA_FOLDER


GEOJSON_POSTCODE = TMP_DATA_FOLDER + "/postcodes.geojson"
GEOJSON_PARISH = TMP_DATA_FOLDER + "/parishes.geojson"
GEOJSON_VOTING_STATION = TMP_DATA_FOLDER + "/valgsteder.geojson"

NAMES_EQUIVALENCE = "data/names_equivalence.csv"
AREA_NAMES_EQUIVALENCES = TMP_DATA_FOLDER + "/area_names.csv"
DISTRICT_NAMES_EQUIVALENCES = TMP_DATA_FOLDER + "/district_names.csv"

# Koordinater for kommunen
longitude_min = 9.721003103433228
longitude_max = 10.394602324249865
latitude_min = 56.845699632588556
latitude_max = 57.1962861090569
MAP_CENTER = {"lat": (latitude_min + latitude_max) / 2, "lon": (longitude_min + longitude_max) / 2}


def download_geojson_file(url: str, file_out: str, reload_if_exists: bool = False) -> None:
    if reload_if_exists or not os.path.exists(file_out):
        with open(file_out, 'w') as f:
            r = requests.get(url, allow_redirects=True)
            geojson = r.json()
            json.dump(geojson, f)


def download_map_backgrounds() -> None:
    """
    Downloads background map data

    :return: /
    """
    # Download af kortdata

    # URL'er til kortdata
    url_sogne = "https://github.com/ok-dk/dagi/raw/master/geojson/sogne.geojson"
    url_postnumre = "https://github.com/ok-dk/dagi/raw/master/geojson/postnumre.geojson"
    url_valgsteder = "https://github.com/ok-dk/geodata/raw/master/geojson/afstemningssteder.geojson"

    # Loader geografiske data for de forskellige postnummer
    if os.path.isdir(TMP_DATA_FOLDER) is False:
        os.mkdir(TMP_DATA_FOLDER)

    download_geojson_file(url_postnumre, GEOJSON_POSTCODE)
    download_geojson_file(url_sogne, GEOJSON_PARISH)
    download_geojson_file(url_valgsteder, GEOJSON_VOTING_STATION)


def plot_map(df: pd.DataFrame, column: str, area: str, area_type: str = "voting_station") -> go.Figure:
    """
    Plots a given dataset on a map

    :param df: dataset to plot (None to make a dummy)
    :param column: name of the column containing the data to plot
    :param area: name of the column designating the area
    :param area_type: type of area ("voting_station", "postcode", "parish")
    :return: plotly figure ready to plot - using .show()
    """

    if area_type in ["valgsted", "voting_station"]:
        with open(GEOJSON_VOTING_STATION, 'r') as f:
            map_background = json.load(f)
        property_name = "AfstemNav"

        # Refining map to only include Aalborg Kommune
        map_features = list()
        for k in map_background["features"]:
            if k["properties"]["KommuneNav"] == "Aalborg":
                map_features.append(k)
        map_background["features"] = map_features
        file_out = DISTRICT_NAMES_EQUIVALENCES

    elif area_type in ["postkode", "postcode"]:
        with open(GEOJSON_POSTCODE, 'r') as f:
            map_background = json.load(f)
        property_name = "POSTNR_TXT"
        file_out = AREA_NAMES_EQUIVALENCES

    elif area_type in ["parish", "sogn"]:
        with open(GEOJSON_PARISH, 'r') as f:
            map_background = json.load(f)
        property_name = "SOGNENAVN"
        file_out = None

    else:
        raise ValueError("Invalid area_type")

    # Merging the different polygons together
    df_geo = gpd.GeoDataFrame.from_features(map_background['features'])
    df_geo_merged = df_geo.dissolve(by=property_name, aggfunc='first')
    df_geo_merged[area] = df_geo_merged.index

    # Creating a fake dataset
    if df is None:
        df = pd.DataFrame(data={area:df_geo_merged.index, column: 1})

    if file_out is not None:
        df_geo_merged.drop(columns=["geometry"]).to_csv(file_out, sep=";")

    # Plotting the map
    min_c = float(df[column].min())
    if min_c<0:
        color_scale = px.colors.diverging.BrBG
        kwargs = dict(color_continuous_midpoint=0)
    else:
        color_scale = "Greens"
        kwargs = dict()

    fig = px.choropleth_mapbox(
        df,
        geojson=df_geo_merged.geometry,
        locations=area,
        color=column,
        color_continuous_scale=color_scale,
        **kwargs,
        mapbox_style="carto-positron",
        zoom=9.5,
        center=MAP_CENTER,
    )

    return fig


def test_map(map_file: str, property_name: str = "AfstemNav"):
    with open(GEOJSON_VOTING_STATION, 'r') as f:
        map_background = json.load(f)

    # Refining map to only include Aalborg Kommune
    map_features = list()
    for k in map_background["features"]:
        if k["properties"]["KommuneNav"] == "Aalborg":
            map_features.append(k)
    map_background["features"] = map_features
    file_out = DISTRICT_NAMES_EQUIVALENCES

    # Merging the different polygons together
    area = "AREA"
    column = "VALUE"
    df_geo = gpd.GeoDataFrame.from_features(map_background['features'])
    df_geo_merged = df_geo.dissolve(by=property_name, aggfunc='first')
    df_geo_merged[area] = df_geo_merged.index

    # Creating a fake dataset
    df = pd.DataFrame(data={area:df_geo_merged.index, column: 1})

    if file_out is not None:
        df_geo_merged.drop(columns=["geometry"]).to_csv(file_out, sep=";")

    # Plotting the map
    color_range = (df[column].min(), df[column].max())
    fig = px.choropleth_mapbox(
        df,
        geojson=df_geo_merged.geometry,
        locations=area,
        color=column,
        color_continuous_scale="Viridis",
        range_color=color_range,
        mapbox_style="carto-positron",
        zoom=9.5,
        center=MAP_CENTER,
        opacity=0.5,
    )
    fig.show()
    return fig

