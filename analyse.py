"""
This file is for generating the maps
"""

import os
from typing import Optional

import pandas as pd
import plotly.graph_objects as go

# --------------------------------------------------------------------------------------------------------------------
from data_loading import load_data_from_json
from data_mapping import download_map_backgrounds, plot_map

# --------------------------------------------------------------------------------------------------------------------

MAPS_FOLDER = "out"
GREEN_ALLIANCE_NAME = "det grønne valgforbund"
df_votes, df_change = load_data_from_json()

try:
    os.mkdir(MAPS_FOLDER)
except:
    pass


def augment_with_green_alliance(df: pd.DataFrame) -> pd.DataFrame:
    green_alliance = ["B", "F", "G", "R", "T", "Ø", "Å"]
    df_out = df.copy()
    df_out[GREEN_ALLIANCE_NAME] = df_out[green_alliance].sum(axis=1)
    return df_out


def __show(fig: go.Figure, out: str) -> None:
    if out == "browser":
        fig.show()
    elif out.endswith(".png"):
        fig.write_image(out, engine="kaleido", width=1900, height=1000)
    else:
        raise ValueError(f"Illegal out={out}")


def plot_percent_vote_per_party(party: str, out: Optional[str] = None) -> None:
    x_percent = party + "_%"
    # Plot per postcode
    df_plot = df_votes.copy()

    if party == GREEN_ALLIANCE_NAME:
        df_plot = augment_with_green_alliance(df_plot)

    df_plot = df_plot.groupby("postcode").sum()
    df_plot["postcode"] = df_plot.index
    df_plot[x_percent] = 100 * df_plot[party] / df_plot["total_votes"]
    fig = plot_map(df_plot, column=x_percent, area="postcode", area_type="postcode")
    fig.update_layout(title=f"Procent stemmer til {party}")
    if out is None:
        out = f"{MAPS_FOLDER}/votes_{party}.png"
    __show(fig, out=out)


def plot_percent_change_per_party(party: str, out: Optional[str] = None) -> None:
    x_percent = party + "_%"
    # Plot per postcode
    df_plot_votes = df_votes.copy()
    df_plot_change = df_change.copy()

    if party == GREEN_ALLIANCE_NAME:
        df_plot_votes = augment_with_green_alliance(df_plot_votes)
        df_plot_change = augment_with_green_alliance(df_plot_change)

    df_plot_votes = df_plot_votes[[party, "postcode"]].groupby("postcode").sum()
    df_plot_change = df_plot_change[[party, "postcode"]].groupby("postcode").sum()
    df_plot = df_plot_votes.merge(
        df_plot_change,
        how="outer",
        left_index=True,
        right_index=True,
        suffixes=("_votes", "_change"),
    )

    party_increase = f"{party} vækst"
    df_plot[party_increase] = (
        100
        * df_plot[party + "_change"]
        / (df_plot[party + "_votes"] - df_plot[party + "_change"])
    )
    df_plot["postcode"] = df_plot.index
    fig = plot_map(
        df_plot, column=party_increase, area="postcode", area_type="postcode"
    )
    fig.update_layout(title=f"Procent ændring i stemmer til {party}")
    if out is None:
        out = f"{MAPS_FOLDER}/ændring_{party}.png"
    __show(fig, out=out)


if __name__ == "__main__":
    print("-- Starting --")
    download_map_backgrounds()
    print("Map backgrounds downloaded")

    for k in list(df_votes.keys()) + [GREEN_ALLIANCE_NAME]:
        print(f"Generating map for {k}")
        if (len(k) > 1) and (k != GREEN_ALLIANCE_NAME):
            continue
        else:
            plot_percent_change_per_party(k)
            plot_percent_vote_per_party(k)
        print(" > DONE")

    print("-- Completed --")

if False:
    plot_percent_change_per_party(GREEN_ALLIANCE_NAME)
    plot_percent_change_per_party("A")
    plot_percent_change_per_party("V")

    plot_percent_vote_per_party(GREEN_ALLIANCE_NAME)
