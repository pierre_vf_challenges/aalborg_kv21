
POETRY_VERSION := 1.8.3

.PHONY: install
install:
	pip install pipx
	pipx ensurepath
	pipx install poetry==$(POETRY_VERSION) || echo "Poetry already installed"
	poetry config virtualenvs.create true 
	poetry install --no-cache
	

.PHONY: run
run:
	python data_loading.py
	python data_mapping.py
	python analyse.py